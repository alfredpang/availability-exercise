import React from 'react';
import ReactDOM from 'react-dom';

import { configure, mount, shallow } from 'enzyme';
import sinon from 'sinon';
import Adapter from 'enzyme-adapter-react-16';

import App from './App';
import { AvailableTimes, AvailableTimesForAdvisor, AvailableTimeBooking, Bookings, BookingsRow } from './App';


configure({ adapter: new Adapter() });

describe('Booking app', () => {
  let mockToday;
  let mockTodayPromise;

  let mockAvailability;
  let mockAvailabilityPromise;

  let mockBookings;
  let mockBookingsPromise;

  let mockBookRequestResponse;
  let mockBookRequestPromise;

  const mockJsonResponse = (status, statusText, response) => {
    return new window.Response(JSON.stringify(response), {
      status: status,
      statusText: statusText,
      headers: {
        'Content-type': 'application/json'
      }
    });
  };

  beforeEach(() => {
    const promiseJsonWrapper = (json) =>
      Promise.resolve({json: () => Promise.resolve(json)});


    mockToday = {
      "today": "2019-04-17"
    };
    mockTodayPromise = Promise.resolve(mockJsonResponse(200, null, mockToday));

    mockAvailability = {
      "availability": {
        "335698": [
          "2019-04-19T15:30:00+00:00",
          "2019-04-19T21:30:00+00:00",
          "2019-04-19T22:00:00+00:00"
        ],
        "372955": [
          "2019-04-23T20:00:00+00:00",
          "2019-04-23T21:30:00+00:00",
          "2019-04-23T22:00:00+00:00",
          "2019-04-23T23:00:00+00:00"
        ]
      }
    };
    mockAvailabilityPromise = Promise.resolve(mockJsonResponse(200, null, mockAvailability));

    mockBookings = {
      "bookings": [
        ["319369", "joey", "2019-04-22T22:00:00+00:00"],
        ["335698", "joey", "2019-04-17T20:00:00+00:00"],
        ["335698", "joey", "2019-04-15T15:00:00+00:00"]]
    };
    mockBookingsPromise = Promise.resolve(mockJsonResponse(200, null, mockBookings));

    mockBookRequestResponse = Object.assign({"result":"ok"}, mockAvailability, mockBookings);
    mockBookRequestPromise = Promise.resolve(mockJsonResponse(200, null, mockBookRequestResponse));

    global.fetch = jest.fn().mockImplementation((url) => {
      const path = url.split("/").pop();
      switch(path) {
      case "today":
        return mockTodayPromise;
      case "availability":
        return mockAvailabilityPromise;
      case "bookings":
        return mockBookingsPromise;
      case "book":
        return mockBookRequestPromise;
      default:
        throw "unknown rest api";
      }
    });
  });

  it('renders without crashing', async () => {
    const div = document.createElement('div');
    const app = ReactDOM.render(<App />, div);

    const getDataPromise = await Promise.all(
      [mockTodayPromise, mockAvailabilityPromise, mockBookingsPromise]);

    // no-op to allow component updates
    // TODO: ensure unmount cancels promises and requests
    await Promise.resolve();

    ReactDOM.unmountComponentAtNode(div);
  });


  it('renders with mount without crashing', async () => {
    const wrapper = mount(<App />);
    await Promise.all([mockTodayPromise, mockAvailabilityPromise, mockBookingsPromise]);

    // no-op to allow update
    await Promise.resolve();
    wrapper.update();

    expect(wrapper.state('name')).toEqual("");
    expect(wrapper.state('availableTimes')).toEqual(mockAvailability.availability);
    expect(wrapper.state('bookings')).toEqual(mockBookings.bookings);

    const availableTimesForAdvisor = wrapper.find('AvailableTimesForAdvisor');
    expect(availableTimesForAdvisor.length).toEqual(2);

    const availableSlots = wrapper.find('AvailableTimeBooking');
    expect(availableSlots.filterWhere((c) => c.props().advisor == "335698").length).toEqual(3);
    expect(availableSlots.filterWhere((c) => c.props().advisor == "372955").length).toEqual(4);

    const bookings = wrapper.find('BookingsRow');
    expect(bookings.length).toEqual(3);

    wrapper.unmount();
  });

  it('click on book button without specifying name and getting alert', async () => {
    const wrapper = mount(<App />);
    await Promise.all([mockTodayPromise, mockAvailabilityPromise, mockBookingsPromise]);

    // no-op to allow update
    await Promise.resolve();
    wrapper.update();

    window.alert = sinon.fake();
    wrapper.update();
    wrapper.find('button.book').first().prop('onClick')()

    // no-op to allow update
    await Promise.resolve();
    wrapper.update();

    expect(window.alert.lastArg).toMatch("Please enter your name before you make a booking!");
    expect(window.alert.callCount).toEqual(1);

    wrapper.unmount();
  });

  it('click on book button after specifying name and getting success', async () => {
    const wrapper = mount(<App />);
    await Promise.all([mockTodayPromise, mockAvailabilityPromise, mockBookingsPromise]);
    wrapper.setState({name: 'joe'});

    // no-op to allow state update
    await Promise.resolve();
    wrapper.update();

    window.alert = sinon.fake();
    wrapper.find('button.book').first().prop('onClick')()

    // no-op to allow update
    await mockBookRequestPromise;
    await Promise.resolve();
    wrapper.update();

    expect(window.alert.callCount).toEqual(1);
    expect(window.alert.lastArg).toMatch("Booked joe with advisor 335698 for 4/19/2019, 11:30:00 AM");

    wrapper.unmount();
  });

  it('availabilty api has server error and failure alert appears', async() => {
    window.alert = sinon.fake();
    mockAvailabilityPromise = Promise.resolve(mockJsonResponse(500, null, {'error': 'all kinds of bad '}));

    const wrapper = mount(<App />);
    await Promise.all([mockTodayPromise, mockAvailabilityPromise, mockBookingsPromise]);

    // no-op to allow update
    await Promise.resolve();
    wrapper.update();

    expect(window.alert.callCount).toEqual(1);
    expect(window.alert.lastArg).toMatch("Unable to get advisor availabilities due to system error. Please try again later!");
  });

});
