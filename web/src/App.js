import React, { Component } from 'react';

// TODO: consider formatting to match original style - 4/3/2019 11:00 am

///////////////////////////////////////////////////////////////
// Availability
///////////////////////////////////////////////////////////////

function AvailableTimes(props) {
  if (props.availableTimes === null) {
    return (
      <div>Loading availability... <p/><p/></div>
    )
  }

  return (
    <table className="advisors table">
      <thead>
        <tr>
          <th>Advisor ID</th>
          <th>Available Times</th>
        </tr>
      </thead>
      <tbody>
         {Object.entries(props.availableTimes).map((times, index) =>
             <AvailableTimesForAdvisor key={times[0]} advisor={times[0]} times={times[1]}
                 onHandleBookAction={props.onHandleBookAction} />
         )}
      </tbody>
    </table>
  );
}

function AvailableTimesForAdvisor(props) {
  return (
    <tr>
      <td>{props.advisor}</td>
      <td>
        <ul className="list-unstyled">
          {props.times.map((dateTime) =>
            <AvailableTimeBooking key={dateTime}
                advisor={props.advisor}
                dateTime={dateTime}
                onHandleBookAction={props.onHandleBookAction} />
          )}
        </ul>
      </td>
    </tr>
  );
}

function AvailableTimeBooking(props) {
  return (
    <li>
      <time dateTime={props.dateTime} className="book-time">
        {new Date(props.dateTime).toLocaleString()}
      </time>
      <button className="book btn-small btn-primary"
          onClick={() => props.onHandleBookAction(props.advisor, props.dateTime)} >Book</button>
    </li>
  );
}

///////////////////////////////////////////////////////////////
// Bookings
///////////////////////////////////////////////////////////////

class Bookings extends Component {
  static _generateBookingKey(row) {
    return "" + row[0] + row[1] + row[2];
  }

  render() {
    if (this.props.bookings === null) {
      return <div>Loading bookings... <p/><p/></div>
    }

    return (
      <table className="bookings table">
        <thead>
          <tr>
            <th>Advisor ID</th>
            <th>Student Name</th>
            <th>Date/Time</th>
          </tr>
        </thead>
        <tbody>
          {this.props.bookings.map((booking, index) =>
            <BookingsRow key={Bookings._generateBookingKey(booking)}
                advisor={booking[0]}
                name={booking[1]}
                dateTime={booking[2]} />
                )}
        </tbody>
      </table>
    );
  }
}

function BookingsRow(props) {
  return (
    <tr>
      <td>{props.advisor}</td>
      <td>{props.name}</td>
      <td>
        <time dateTime={props.dateTime}>
          {new Date(props.dateTime).toLocaleString()}
        </time>
      </td>
    </tr>
  );
}

///////////////////////////////////////////////////////////////
// Main App
///////////////////////////////////////////////////////////////

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      availableTimes: null,
      bookings: null,
    };
    this._handleNameChange = this._handleNameChange.bind(this);
    this._handleNameSubmit = this._handleNameSubmit.bind(this);
    this._handleBookAction = this._handleBookAction.bind(this);
  }

  componentDidMount() {
    this._fetchToday();
    this._fetchAvailableTimes();
    this._fetchBookings();
  }

  componentWillUnmount() {
    // TODO: cancel promises
    // TODO: cancel network requests
  }

  async _fetchToday() {
    try {
      const res = await fetch("http://localhost:4433/today");
      const json = await res.json();
      this.setState({today: json.today});
    } catch (e) {
      console.error("Failed to fetch 'today' data", e);
    }
  }

  async _fetchAvailableTimes() {
    try {
      const options = {method: "POST"};
      const res = await fetch("http://localhost:4433/availability", options);
      if (res.status !== 200) {
        alert("Unable to get advisor availabilities due to system error. Please try again later!");
        return;
      }

      const json = await res.json();
      this.setState({availableTimes: json.availability});
    } catch (e) {
      console.error("Failed to fetch availability", e);
      alert("Unable to get advisor availabilities due to network error. Please try again later!");
    }
  }

  async _fetchBookings() {
    try {
      const options = {method: "POST"};
      const res = await fetch("http://localhost:4433/bookings", options);
      if (res.status !== 200) {
        alert("Unable to get bookings due to system error. Please try again later!");
        return;
      }

      const json = await res.json();
      this.setState({bookings: json.bookings});
    } catch (e) {
      console.error("Failed to fetch bookings", e);
      alert("Unable to get bookings due to network error. Please try again later!");
    }
  }

  async _postBookRequest({name, advisor, dateTime}) {
    const options = {
      method: "POST",
      body: JSON.stringify({name, advisor, dateTime}),
      headers: {
        'Content-Type': 'application/json'
      }
    };

    try {
      const res = await fetch("http://localhost:4433/book", options);
      const json = await res.json();

      if (res.status !== 200) {
        alert("Unable to make booking: " + json.error);
        console.error("booking request failed", res)
        return;
      }

      this.setState({availableTimes: json.availability});
      this.setState({bookings: json.bookings});

      alert("Booked " + name + " with advisor " + advisor + " for " +
          new Date(dateTime).toLocaleString());

    } catch (e) {
      console.error("Unable to make booking request", e)
      alert("Unable to make booking due to network error. Please try again later!");
    }
  }

  _handleNameChange(e) {
    this.setState({name: e.target.value});
  }

  _handleNameSubmit(e) {
    alert("We will book appointments for " + this.state.name);
    e.preventDefault();
  }

  _handleBookAction(advisor, dateTime) {
    if (!this.state.name) {
      alert("Please enter your name before you make a booking!");
      return;
    }
    this._postBookRequest({name: this.state.name.trim(), advisor, dateTime});
  }

  render() {
    return (
      <div className="App container">
        <h1>Book Time with an Advisor</h1>

        {this.state.today && <span id="today">Today is {this.state.today}.</span>}

        <form id="name-form" className="col-md-6" onSubmit={this._handleNameSubmit}>
          <div className="form-group">
            <label htmlFor="name-field">Your Name</label>
            <input type="text" id="name-field" className="form-control"
                value={this.state.name} onChange={this._handleNameChange} />
          </div>
        </form>

        <h2>Available Times</h2>
        <AvailableTimes availableTimes={this.state.availableTimes}
            onHandleBookAction={this._handleBookAction} />

        <h2>Booked Times</h2>
        <Bookings bookings={this.state.bookings} />
      </div>
    );
  }
}

export default App;
export { AvailableTimes, AvailableTimesForAdvisor, AvailableTimeBooking, Bookings, BookingsRow };
