from datetime import date
from datetime import datetime
from datetime import timezone
import json

import pytest
import requests
import requests_mock as rm_module

from app import AVAIL_API
from app import Bookings
from app import g_all_bookings
from app import generate_availability
from app import app

@pytest.fixture(autouse=True)
def reset_for_each_test():
    g_all_bookings.clear()

@pytest.fixture
def requests_mock(request):
    m = rm_module.Mocker()
    m.start()
    request.addfinalizer(m.stop)
    return m

def test_today():
    with app.test_client() as cli:
        resp = cli.get('/today')
        assert resp.status_code == 200
        assert resp.json == {"today": "{}".format(date.today())}

def test_bookings():
    a_booking_time = datetime.fromisoformat("2019-01-02T03:00:00+08:00")
    a_booking_advisor = 1000
    a_booking_user = "a"
    a_booking = (a_booking_user, a_booking_time, a_booking_advisor)

    bookings = Bookings()
    bookings.add_booking(*a_booking)

    assert bookings.get_booking_by_time(a_booking_time, a_booking_advisor) == a_booking_user
    assert bookings.get_booking_by_time(a_booking_time, 2000) is None

    assert len(bookings.by_time_advisor_for_name)

    bookings_json_data = bookings.as_json()
    assert len(bookings_json_data) == 1

    booking_result = bookings_json_data[0]
    assert booking_result[0] == str(a_booking_advisor)
    assert booking_result[1] == a_booking_user
    assert booking_result[2] == a_booking_time.astimezone(timezone.utc).isoformat()

def test_generate_availability():
    # NOTE: using time as key prevents multiple advisors to be available at
    # the same timeslot
    downstream_avail = {
        "2019-04-01": {
          "2019-04-01T12:00:00-04:00": 1000,
          "2019-04-01T12:30:00-04:00": 1000,
          "2019-04-01T13:00:00-04:00": 1000,
          "2019-04-01T15:00:00-04:00": 3000,
        }
    }

    a_booking_time = datetime.fromisoformat("2019-04-01T13:00:00-04:00")
    a_booking_advisor = 1000
    a_booking_user = "a"
    a_booking = (a_booking_user, a_booking_time, a_booking_advisor)

    g_all_bookings.add_booking(*a_booking)
    avail_json_result = json.dumps(generate_availability(downstream_avail))

    expected_json_result = '{"availability": {"1000": ["2019-04-01T16:00:00+00:00", "2019-04-01T16:30:00+00:00"], "3000": ["2019-04-01T19:00:00+00:00"]}}'
    assert avail_json_result == expected_json_result

def test_generate_availability_looking_for_slot_found():
    downstream_avail = {
        "2019-04-01": {
          "2019-04-01T12:00:00-04:00": 1000,
          "2019-04-01T12:30:00-04:00": 1000,
          "2019-04-01T13:00:00-04:00": 1000,
          "2019-04-01T15:00:00-04:00": 3000,
        }
    }

    a_booking_time = datetime.fromisoformat("2019-04-01T13:00:00-04:00")
    a_booking_advisor = "1000"

    avail_json_result, requested_found = generate_availability(downstream_avail, a_booking_advisor, a_booking_time)

    expected_json_result = '{"availability": {"1000": ["2019-04-01T16:00:00+00:00", "2019-04-01T16:30:00+00:00"], "3000": ["2019-04-01T19:00:00+00:00"]}}'

    assert json.dumps(avail_json_result) == expected_json_result
    assert requested_found

def test_generate_availability_looking_for_slot_not_found():
    downstream_avail = {
        "2019-04-01": {
          "2019-04-01T12:00:00-04:00": 1000,
          "2019-04-01T12:30:00-04:00": 1000,
          "2019-04-01T13:00:00-04:00": 1000,
          "2019-04-01T15:00:00-04:00": 3000,
        }
    }

    a_booking_time = datetime.fromisoformat("2019-10-10T09:00:00-04:00")
    a_booking_advisor = "5000"

    avail_json_result, requested_found = generate_availability(downstream_avail, a_booking_advisor, a_booking_time)

    expected_json_result = '{"availability": {"1000": ["2019-04-01T16:00:00+00:00", "2019-04-01T16:30:00+00:00", "2019-04-01T17:00:00+00:00"], "3000": ["2019-04-01T19:00:00+00:00"]}}'
    assert json.dumps(avail_json_result) == expected_json_result
    assert not requested_found

def test_availabilty_route(requests_mock):
    with app.test_client() as cli:
        downstream_avail = {
          "2019-04-01": {
            "2019-04-01T12:00:00-04:00": 2000,
            "2019-04-01T12:30:00-04:00": 1000,
            "2019-04-01T13:00:00-04:00": 1000,
            "2019-04-01T15:00:00-04:00": 3000
          }
        }

        requests_mock.get(AVAIL_API, json=downstream_avail)
        resp = cli.post('/availability')
        assert resp.status_code == 200

        expected_result = '{"1000": ["2019-04-01T16:30:00+00:00", "2019-04-01T17:00:00+00:00"], "2000": ["2019-04-01T16:00:00+00:00"], "3000": ["2019-04-01T19:00:00+00:00"]}'
        assert json.dumps(resp.json['availability']) == expected_result

def test_availabilty_route_bad(requests_mock):
    with app.test_client() as cli:
        requests_mock.get(AVAIL_API, status_code=404)
        resp = cli.post('/availability')
        assert resp.status_code == 500
        assert resp.json['error']

def test_availabilty_route_malformed_json(requests_mock):
    with app.test_client() as cli:
        requests_mock.get(AVAIL_API, text='{"a": 1234')
        resp = cli.post('/availability')
        assert resp.status_code == 500
        assert resp.json['error']

def test_bookings():
    a_booking_time = datetime.fromisoformat("2019-04-01T13:00:00-04:00")
    a_booking_advisor = 1000
    a_booking_user = "a"
    a_booking = (a_booking_user, a_booking_time, a_booking_advisor)

    b_booking_time = datetime.fromisoformat("2019-04-01T14:00:00-04:00")
    b_booking_advisor = 2000
    b_booking_user = "b"
    b_booking = (b_booking_user, b_booking_time, b_booking_advisor)

    g_all_bookings.add_booking(*a_booking)
    g_all_bookings.add_booking(*b_booking)

    with app.test_client() as cli:
        resp = cli.post('/bookings')
        assert resp.status_code == 200
        assert resp.json['bookings'] == [['1000', 'a', '2019-04-01T17:00:00+00:00'], ['2000', 'b', '2019-04-01T18:00:00+00:00']]

def test_book_normal(requests_mock):
    with app.test_client() as cli:
        avail = {
          "2019-04-01": {
            "2019-04-01T17:00:00+00:00": 1000,
            "2019-04-01T19:00:00+00:00": 2000
          }
        }
        requests_mock.get(AVAIL_API, json=avail)

        a_booking = {
            'name': 'a',
            'advisor': '1000',
            'dateTime': '2019-04-01T17:00:00+00:00'}
        resp = cli.post('/book', json=a_booking)
        assert resp.status_code == 200
        assert resp.json['result'] == "ok"
        assert resp.json['availability'] == {'2000': ['2019-04-01T19:00:00+00:00']}
        assert resp.json['bookings'] == [['1000', 'a', '2019-04-01T17:00:00+00:00']]

def test_book_collide(requests_mock):
    with app.test_client() as cli:
        avail = {
          "2019-04-01": {
            "2019-04-01T17:00:00+00:00": 1000
          }
        }
        requests_mock.get(AVAIL_API, json=avail)

        a_booking = {
            'name': 'a',
            'advisor': '1000',
            'dateTime': '2019-04-01T17:00:00+00:00'}
        resp = cli.post('/book', json=a_booking)
        assert resp.status_code == 200
        assert resp.json['result'] == "ok"

        # first booking was okay
        # second one should fail
        resp = cli.post('/book', json=a_booking)
        print(resp.json)
        assert resp.status_code == 400   # client's fault 4xx

        assert g_all_bookings.as_json() == [('1000', 'a', '2019-04-01T17:00:00+00:00')]

def test_book_no_longer_available(requests_mock):
    with app.test_client() as cli:
        avail = {
          "2019-09-01": {
            "2019-09-01T17:00:00+00:00": 1000
          }
        }
        requests_mock.get(AVAIL_API, json=avail)

        a_booking = {
            'name': 'a',
            'advisor': '1000',
            'dateTime': '2019-04-01T17:00:00+00:00'}
        resp = cli.post('/book', json=a_booking)
        print(resp.json)

        assert resp.status_code == 500
        assert resp.json['error']

def test_book_malformed():
    with app.test_client() as cli:
        a_booking = {
            'name': 'a',
            'advisor': '1000',
            'dateTime': '2019-04-'}
        resp = cli.post('/book', json=a_booking)
        assert resp.status_code == 400    # client sending bad stuff is 4xx
        assert resp.json['error']
        assert g_all_bookings.as_json() == []
