from flask import Flask, jsonify, request
from flask_cors import CORS

from collections import OrderedDict
from datetime import date
from datetime import datetime
from datetime import timezone
import requests

AVAIL_API = "https://www.thinkful.com/api/advisors/availability"

app = Flask(__name__)
CORS(app)

#
# Internal store of bookings
#
class Bookings:
    def __init__(self):
        self._by_time_advisor_for_name = {}  # (time, advisior) -> name

    def clear(self):
        self._by_time_advisor_for_name = {}  # (time, advisior) -> name

    # just a list of tuples (advisor, name, time_iso_formatted)
    # nicely for the client consumption
    def as_json(self):
        return [(dt_advisor[1], name, dt_advisor[0].isoformat())
            for dt_advisor, name in self._by_time_advisor_for_name.items()]

    def _load_fake_data(self):
        self.add_booking(
          "Smith Hichew", datetime.fromisoformat("2019-02-02T10:00:00-04:00"), 902109)

    # return name of user booking for (time, advisor) tuple
    def get_booking_by_time(self, time, advisor):
        return self._by_time_advisor_for_name.get((time.astimezone(timezone.utc), str(advisor)))

    # success or throws
    def add_booking(self, name, time, advisor):
        time = time.astimezone(timezone.utc)
        booking = (time, str(advisor))

        # double check
        if booking in self._by_time_advisor_for_name:
            raise Exception('this booking has already been made by someone else')

        self._by_time_advisor_for_name[booking] = name

# module global to store bookings
g_all_bookings = Bookings()

#
# /availability - using downstream availability + bookings, generate json
# /book - generate availabity and remove requested booking if it is in there
#
def generate_availability(raw_avail, requested_advisor=None, requested_time=None):
    # raw availability from downstream looks like this:
    # {
    #   "2019-04-16": {
    #     "2019-04-16T19:00:00-04:00": 424108,
    #   ...
    # }

    requested_advisor = str(requested_advisor) if requested_advisor else None
    requested_time = requested_time.astimezone(timezone.utc) if requested_time else None

    found_requested = False
    by_advisor = OrderedDict()

    for date_groups in raw_avail.values():
        for avail_time_str, advisor_int in date_groups.items():
            advisor = str(advisor_int)
            avail_time = datetime.fromisoformat(avail_time_str).astimezone(timezone.utc)

            if g_all_bookings.get_booking_by_time(avail_time, advisor) is not None:
                continue

            if (requested_advisor, requested_time) == (advisor, avail_time):
                found_requested = True
                continue

            # convert time back to ISO GMT string to send to client
            by_advisor.setdefault(advisor, []).append(avail_time.isoformat())

    for avail_times in by_advisor.values():
        avail_times.sort()

    if requested_advisor is None:
      return {'availability': by_advisor}
    else:
      return {'availability': by_advisor}, found_requested

# {
#   "result": "ok",
#   "today": "iso date"
# }
@app.route("/today", methods=["GET"])
def today():
    return jsonify({"today": date.today().isoformat()})

# {
#   "result": "ok",
#   "availability": {
#     "424108": ["2019-04-17T16:00:00-04:00", ...],
#     ...
#   }
# }
@app.route("/availability", methods=["POST"])
def availability():
    try:
        r = requests.get(AVAIL_API)
        if r.status_code != 200:
            return jsonify({"error": "downstream error"}), 500

        return jsonify(generate_availability(r.json()))
    except Exception as err:
        # general badness
        return jsonify({"error": str(err)}), 500

# {
#   "result": "ok",
#   "bookings": [
#     [2345, "joey", "2019-04-17T16:00:00-04:00"],
#     ...
#   ]
@app.route("/bookings", methods=["POST"])
def bookings():
    return jsonify(dict(bookings=g_all_bookings.as_json()))

# {
#   "result": "ok",
# }
@app.route("/book", methods=["POST"])
def book():
    try:
        data = request.get_json()
        name = data.get('name', '').strip()
        advisor = data.get('advisor')
        booking_datetime = datetime.fromisoformat(data.get('dateTime')).astimezone(timezone.utc)

        if not name or not advisor or not booking_datetime:
            return jsonify({"error": "missing name, advisor or dateTime"}), 400

        if g_all_bookings.get_booking_by_time(booking_datetime, advisor) is not None:
            return jsonify({"error": "this booking has already been taken"}), 400

        r = requests.get(AVAIL_API)
        if r.status_code != 200:
            return jsonify({"error": "downstream error"}), 500

        resp_json, requsted_found = generate_availability(r.json(), advisor, booking_datetime)

        if not requsted_found:
            return jsonify({"error": "requested availability no longer available. you might want to refresh your browser and see the new availability"}), 500

        try:
            g_all_bookings.add_booking(name, booking_datetime, advisor)
        except Exception as err:
            return jsonify({"error": str(err)}), 500

        resp_json['result'] = 'ok'
        resp_json['bookings'] = g_all_bookings.as_json()

        return jsonify(resp_json)

    except ValueError as err:
        return jsonify({"error": str(err)}), 400

    except Exception as err:
        return jsonify({"error": str(err)}), 500

